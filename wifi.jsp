<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.io.*, java.text.*" %>
<%		
	// 선택된 페이지 번호 참조
	String pageNum=request.getParameter("pageNum");
	if(pageNum==null){
		pageNum="1";
	}	
	int currentPage=Integer.parseInt(pageNum);	
	
	// 한페이지에 보일 레코드 숫자
	String pageSizeS=request.getParameter("pageSize");
	if(pageSizeS==null){
		pageSizeS="30";
	}	
	int pageSize=Integer.parseInt(pageSizeS);
	// int pageSize = 32;
	
	String lat=request.getParameter("lat");
	// if(lat.equals(null)){
		// lat="37.386052";
	// } 
	double latitude;
	try {
		latitude=Double.parseDouble(lat);
	} catch(Exception e) {
		latitude=37.3860521;
	}
	// double latitude=Double.parseDouble(lat);
	
	String lng=request.getParameter("lng");
	// if(lng.equals(null)){
		// lng="127.121403";
	// }
	// double longitude=Double.parseDouble(lng);
	double longitude;
	try {
		longitude=Double.parseDouble(lng);
	} catch(Exception e) {
		longitude=127.1214038;
	}
%>

<%!
	private static double distFromFunc(double lat1, double lng1, double lat2, double lng2) {
		double theta = lng1 - lng2;
		double distance = Math.sin(lat1*Math.PI/180.0) * Math.sin(lat2*Math.PI/180.0)
				+ Math.cos(lat1*Math.PI/180.0) * Math.cos(lat2*Math.PI/180.0) * Math.cos(theta*Math.PI/180.0);

		distance = Math.acos(distance);
		distance = distance*180 / Math.PI;
		distance = distance * 60 * 1.1515;
		distance = distance * 1.609344;		// km
		distance = Math.round(distance*100)/100d;
		
		return distance;
	}
%>
<html>
<head>
	<title>가까운 무료와이파이</title>
</head>
<body>
<center>
<%	
	File f = new File("/var/lib/tomcat8/webapps/ROOT/ch04_wifi/wifi.txt");    
	BufferedReader br = new BufferedReader(new FileReader(f));
	String readText;
	if((readText = br.readLine()) == null){
		System.out.println("빈 파일입니다.<br>");
		br.close();
		return;
	}
	
	String[] field =readText.split("/t");
	String[] maxLine = {};
	String[] minLine = {};
	double dMax = 0;
	double dMin = 10000000;
	double distFrom = 0;
	
	// 총 record 수 저장 변수
	int LineCnt =0; 
	
	while((readText = br.readLine()) != null){	
		LineCnt++;
		field=readText.split("\t");
		
		double dist=Math.sqrt(Math.pow(Double.parseDouble(field[12])-latitude,2)
					+Math.pow(Double.parseDouble(field[13])-longitude,2));
		distFrom = distFromFunc(Double.parseDouble(field[12]), Double.parseDouble(field[13]), latitude, longitude);
		if (dMax < dist) {
			maxLine=field;
			dMax = dist;
		}
		if (dMin > dist) {
			minLine=field;
			dMin = dist;
		}
	}
	br.close();
	int totalRecords=LineCnt;	// 총 레코드 수 확인
	
	
	
	// 총 페이지 수 계산
	int totalPage = 0;							// 총 페이지 변수 설정
	int R = totalRecords % pageSize;			
	if (R == 0){	
		totalPage=totalRecords/pageSize;		// 총 페이지 수 결정
	} else {
		totalPage=totalRecords/pageSize +1;		// 총 페이지 수 결정(나머지가 다를 경우)
	}

	int groupSize = 10;			// 한 그룹의 페이지 개수
	int currentGroup = 0;		// 현재 그룹 설정 초기화


	R = totalPage%groupSize;									// 현재 속한 그룹 설정
	if (R==0){
		currentGroup=currentPage/groupSize;
	} else {
		currentGroup=currentPage/groupSize+1;
	}
	
	currentGroup = (int)Math.floor((currentPage-1)/ groupSize);	// 현재 그룹 
	int groupStartPage = currentGroup*groupSize +1;				// 현 그룹 시작 페이지
	int groupEndPage = groupStartPage+groupSize-1;				// 현 그룹 끝 페이지
	
	if(groupEndPage>totalPage){									// 마지막 페이지 설정
		groupEndPage = totalPage;
	}	


	File f2=new File("/var/lib/tomcat8/webapps/ROOT/ch04_wifi/wifi.txt");
	BufferedReader br2=new BufferedReader(new FileReader(f2));
	
	// String readText;	
	
	if((readText=br2.readLine())==null){
		out.println("빈 파일입니다.<br/>");
		br2.close();
		return;
	}
	
	String[] fieldName=readText.split("\t");	
	int lineCount=0;	
%>

<table border = "1" cellspacing=0 width="1000">
	<tr><td colspan="6" align="center">전국무료와이파이데이터</td></tr>
	<tr><td colspan="6"></td></tr>
	<tr>
		<td width="50"  align="center">번호</td>
		<td width="450" align="center"><%=fieldName[9]%></td>
		<td width="100" align="center"><%=fieldName[12]%></td>
		<td width="100" align="center"><%=fieldName[13]%></td>
		<td width="150" align="center">거리</td>
		<td width="150" align="center">거리(km)</td>
	</tr>	
	
<%	
	int fromPT =(currentPage-1)*pageSize+1; //시작 번호
	
	out.println("<br>");		
	
	while((readText=br2.readLine())!=null){		
		
		lineCount++;		
		if(lineCount < fromPT) continue;
		if(lineCount >= fromPT+pageSize) break;
		
		field=readText.split("\t");
		// maxLine=field;
		// minLine=field;
		
		out.println("<tr><td>"+lineCount+"</td>");
		out.println("<td>"+field[9]+"</td>");
		out.println("<td align=center>"+field[12]+"</td>");
		out.println("<td align=center>"+field[13]+"</td>");
		double dist=Math.sqrt(Math.pow(Double.parseDouble(field[12])-latitude,2)
					+Math.pow(Double.parseDouble(field[13])-longitude,2));

		distFrom = distFromFunc(Double.parseDouble(field[12]), Double.parseDouble(field[13]), latitude, longitude);
		out.println("<td align=center>"+dist+"</td>");	
		out.println("<td align=center>"+distFrom+"</td></tr>");	
	}			
	br2.close();	
%>
	<tr><td colspan="6"></td></tr>
	<tr>
		<td colspan="6" align="center">
		<a href="wifiTestUI3.jsp?pageNum=<%=1%>" style="text-decoration:none">First</a>&nbsp&nbsp;
<%			
	if(currentGroup >= 1){
%>
		<a href="wifiTestUI3.jsp?pageNum=<%=groupStartPage-1%>" style="text-decoration:none">Front</a>&nbsp&nbsp;
<%		
	}

	int groupPageCount = groupSize;		// 그룹 당 페이지 수
	int index = groupStartPage;			// 현 그룹 시작 페이지	

	// 그룹당 페이지수가 0보다 크고 && 페이지 수가 그룹의 마지막 페이지 번호보다 작거나 같을때 반복문 실행
	while(groupPageCount>0 && index<=groupEndPage){				
%>
		<!-- <a href="wifiTestUI3.jsp?pageNum=< %=index%>" style="text-decoration:none">< %=index%></a>&nbsp&nbsp; -->
<%	
		if(index==currentPage){
%>
			<b>[&nbsp<a href="wifiTestUI3.jsp?pageNum=<%=index%>" style="text-decoration:none"><%=index%></a>&nbsp]&nbsp&nbsp;</b>
<%			
		} else {
%>
			<a href="wifiTestUI3.jsp?pageNum=<%=index%>" style="text-decoration:none"><%=index%></a>&nbsp&nbsp;
<%			
		}
		index = index+1;					// 페이지 번호 1씩 증가
		groupPageCount=groupPageCount-1;	// 읽어야 할 그룹 페이지 하나씩 감소
	}

	if(index<=totalPage){
%>
		<a href="wifiTestUI3.jsp?pageNum=<%=index%>" style="text-decoration:none">Next</a>&nbsp&nbsp;
<%	
	}
	
	if(currentGroup >= 0){
%>
		<a href="wifiTestUI3.jsp?pageNum=<%=totalPage%>" style="text-decoration:none">Last</a>&nbsp;
<%		
	}
%>
		</td>
	<tr>
</table>


<table>
	<form method="post" action="wifiTestUI3.jsp">
	<tr><td>
		latitude&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="latitude" name="lat"><br>
	<td></tr>

	<tr><td>
		longitude&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: <input type="longitude" name="lng"><br>
		<input type="submit" value="submit">
	<td></tr>
	</form>
</table>


<table border = "0" cellspacing=0 width="1000">
<tr>
	<td align=right>입력한 위도&nbsp&nbsp</td>
	<td colspan="5">&nbsp&nbsp<%=latitude%></td>
</tr>
<tr>
	<td align=right>입력한 경도&nbsp&nbsp</td>
	<td colspan="5">&nbsp&nbsp<%=longitude%></td>
</tr>
</table>
<br>

<table border = "1" cellspacing=0 width="1000">
	<tr><td colspan="6" align="center">입력한 위치로부터 가장 가까운 무료와이파이</td></tr>
	<tr><td colspan="6" align="center"></td></tr>
	<tr>	
	<td width="450" align="center">주소</td>	
	<td width="100" align="center">위도</td>
	<td width="100" align="center">경도</td>
	<td width="150" align="center">거리</td>
	<td width="150" align="center">거리(km)</td>
	</tr>
<%
	out.println("<tr><td>"+minLine[9]+"</td>");
	out.println("<td align=center>"+minLine[12]+"</td>");	
	out.println("<td align=center>"+minLine[13]+"</td>");
	double minD=Math.sqrt(Math.pow(Double.parseDouble(minLine[12])-latitude,2)
					+Math.pow(Double.parseDouble(minLine[13])-longitude,2));
	out.println("<td align=center>"+minD+"</td>");
	distFrom = distFromFunc(Double.parseDouble(minLine[12]), Double.parseDouble(minLine[13]), latitude, longitude);
	out.println("<td align=center>"+distFrom+"</td>");
%>	
</table>
</center>

</body>
</html>